import requests
import json
import re
import urllib
from urllib.request import urlopen
from urllib.parse import urlparse, quote_plus
from flask import Flask
from parsel import Selector
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


IMOVIRTUAL_XPATH = {
    "caracteristics": "//*[@id=\"__next\"]/main/div/div[3]/ul/li/text()",
    "description": "//*[@id=\"__next\"]/main/div/div[3]/section[2]/div/div/text()",
    "location": "//*[@id=\"__next\"]/main/div/div[2]/header/div[2]/a/text()",
    "numberBathrooms": "//*[@id=\"__next\"]/main/div/div[3]/div[1]/div[4]/div[2]/text()",
    "price": "//*[@id=\"__next\"]/main/div/div[2]/header/strong/text()",
    "sellerName": "//*[@id=\"__next\"]/main/div/aside/div[4]/strong/text()",
    "sellerPhoneNumber": "//*[@id=\"__next\"]/main/div/aside/div[4]/div/span/text()",
    "title": "//*[@id=\"__next\"]/main/div/div[2]/header/h1/text()",
    "typo": "//*[@id=\"__next\"]/main/div/div[3]/div[1]/div[3]/div[2]/text()",
    "utilArea": "//*[@id=\"__next\"]/main/div/div[3]/div[1]/div[1]/div[2]/text()"
}


XPATH_SELECTORS = {
    "caracteristics": {
        "idealista.pt": "//*[@id=\"details\"]/div/div[1]/div/ul/li/text()",
        "imovirtual.com": "//*[@id=\"__next\"]/main/div/div[3]/ul/li/text()",
        "fraccaoexacta.com": ""
    },
    "description": {
        "idealista.pt": "//*[@id=\"main\"]/div/main/section[1]/div[8]/div[2]/div[1]/text()",
        "imovirtual.com":"//*[@id=\"__next\"]/main/div/div[3]/section[2]/div/div/text()",
        "fraccaoexacta.com": "//*[@id=\"groupDescription\"]/div/p/text()"
    },
    "image": {
        "idealista.pt": "//*[@id=\"main\"]/div/main/picture/div/img/@src",
        #"imovirtual.com": "//*[@id=\"__next\"]/main/div/div[3]/section[1]/div/div/div/div/div[1]/div[1]/div/div[1]/div/picture/img/@src/text()",
        "imovirtual.com": "//img[@class=\"image-gallery-thumbnail-image\"]/@src/text()",
        "fraccaoexacta.com": "//*[@id=\"ctl00_ContentBody_ucDetail_repMainPhotos_ctl01_imgThumbnail\"]/text()"
    },
    "location": {
        "idealista.pt": "//*[@id=\"main\"]/div/main/section[1]/div[2]/span/span/text()",
        "imovirtual.com":"//*[@id=\"__next\"]/main/div/div[2]/header/div[2]/a/text()",
        "fraccaoexacta.com": "//*[@id=\"ctl00_ContentBody_ucDetail_pnlDetail\"]/section[1]/div/div[1]/p/text()"
    },
    "numberBathrooms": {
        "idealista.pt": "//*[@id=\"details\"]/div/div[1]/div/ul/li[1]/text()",
        "imovirtual.com":"//*[@id=\"__next\"]/main/div/div[3]/div[1]/div[4]/div[2]/text()",
        "fraccaoexacta.com": "//*[@id=\"ctl00_ContentBody_ucDetail_lblBaths\"]/text()"
    },
    "price": {
        "idealista.pt": "//*[@id=\"mortgages\"]/div[2]/div/article/section/p[1]/strong/text()",
        "imovirtual.com":"//*[@id=\"__next\"]/main/div/div[2]/header/strong/text()",
        "fraccaoexacta.com": "//*[@id=\"ctl00_ContentBody_ucDetail_pnlDetail\"]/section[1]/div/div[2]/h2/text()"
    },
    "sellerName": {
        "idealista.pt": "//*[@id=\"side-content\"]/section/div/div[6]/div[4]/a/text()",
        "imovirtual.com":"//*[@id=\"__next\"]/main/div/aside/div[4]/strong/text()",
        "fraccaoexacta.com": "//*[@id=\"ctl00_ContentBody_ucDetail_ctrAgency\"]/div/div/div/div/div[2]/h4/text()"
    },
    "sellerPhoneNumber": {
        "idealista.pt": "//*[@id=\"side-content\"]/section/div/div[6]/div[1]/div/div/p/text()",
        "imovirtual.com":"//*[@id=\"__next\"]/main/div/aside/div[4]/div/span/text()",
        "fraccaoexacta.com": "//*[@id=\"ctl00_ContentBody_ucDetail_lblMobile\"]/text()"
    },
    "title": {
        "idealista.pt": "//*[@id=\"main\"]/div/main/section[1]/div[2]/h1/span/text()",
        #"imovirtual.com":"//*[@id=\"__next\"]/main/div/div[2]/header/h1/text()",
        "imovirtual.com":"//h1[@data-cy=\"adPageAdTitle\"]/text()",
        "fraccaoexacta.com": "//*[@id=\"ctl00_ContentBody_ucDetail_pnlDetail\"]/section[1]/div/div[1]/h1/span/text()"
    },
    "typo": {
        "idealista.pt": "//*[@id=\"main\"]/div/main/section[1]/div[5]/span[2]/span/text()",
        "imovirtual.com":"//*[@id=\"__next\"]/main/div/div[3]/div[1]/div[3]/div[2]/text()",
        "fraccaoexacta.com": ""
    },
    "utilArea": {
        "idealista.pt": "//*[@id=\"main\"]/div/main/section[1]/div[5]/span[1]/text()",
        "imovirtual.com":"//*[@id=\"__next\"]/main/div/div[3]/div[1]/div[1]/div[2]/text()",
        "fraccaoexacta.com": ""
    }
}
WEBSITES = ["imovirtual.com"]

SEARCH_PAGE = {
    "idealista.pt": {
        "selector": "//*[@id=\"main-content\"]/section[1]/article[1]/div/a/@href",
        "addDomain": True,
        "needsProxy": True,
        "scheme": "https"
    },
    "imovirtual.com": {
        "selector": "//header[@class='offer-item-header\n            ']/h3/a/@href",
        "addDomain": False,
        "needsProxy": False,
        "scheme": "https"
    },
    "olival-imobiliaria.pt": {
        "selector": "//div[@class=\"propertyItems\"]/div[1]/div[3]/div/a/@href/text()",
        "addDomain": False,
        "needsProxy": True,
        "scheme": "http"
    },
    "fraccaoexacta.com": {
        "selector": "//a[@class='property-item']/@href",
        "addDomain": True,
        "needsProxy": False,
        "scheme": "https"
    }
}

def buildURL(website: str, location, type):
    if website == "idealista.pt":
        return ""
    elif website == "imovirtual.com":
        return ""
    elif website == "olival-imobiliaria.pt":
        return ""
    elif website == "fraccaoexacta.com":
        return ""
# OrN5vu7iVOQlO8v1EvLyCQ Javascript
@app.route("/")
@cross_origin()
def index():
    result = []
    search_listing_url_list = []
    search_listing_url_list.append("https://www.imovirtual.com/comprar/apartamento/porto/?search%5Bregion_id%5D=13")
    #url = "https://www.idealista.pt/comprar-casas/lisboa/"
    #url_str = "http://www.olival-imobiliaria.pt/imoveis/loures"
    search_listing_url_list.append("https://www.fraccaoexacta.com/imoveis/apartamentos/portugal/")
    for search_listing_url in search_listing_url_list:
        website = '.'.join(urlparse(search_listing_url).netloc.split('.')[1:])
        print("Website: ",website)
        
        if SEARCH_PAGE[website]["needsProxy"]:
            #handler = urlopen('https://api.proxycrawl.com/?token=muaEhrtVNUlQJo1aCXX5IQ&url=' + quote_plus(url_str))
            #page = handler.read()
            page = requests.get('https://api.proxycrawl.com/?token=muaEhrtVNUlQJo1aCXX5IQ&url=' + quote_plus(search_listing_url), proxies=urllib.request.getproxies(), verify=False).text
            print(page)
        else:
            page = requests.get(search_listing_url, proxies=urllib.request.getproxies(), verify=False).text
        #print("Page: ",page.text)
        selector_url = SEARCH_PAGE[website]["selector"]
        # *[@id=\"offer-item-ad_id15VxV\"]/div[1]/header/h3/a/@href
        search_pages = Selector(page).xpath(selector_url).getall()
        print("Search Pages: ", search_pages)
        for listing_page_url in search_pages:
            print("Website: ", website)
            listing_page_url = SEARCH_PAGE[website]["scheme"] + "://" + website + listing_page_url if SEARCH_PAGE[website]["addDomain"] else listing_page_url
            print("URL: ", listing_page_url)
            page = requests.get(listing_page_url)
            selector = Selector(page.text)
            if selector.xpath(XPATH_SELECTORS["title"][website]).getall() == []:
                continue
            ad = {}
            for element in XPATH_SELECTORS:
                if XPATH_SELECTORS[element][website]:
                    ad[element] = selector.xpath(XPATH_SELECTORS[element][website]).getall()
                    map(lambda x: re.sub("^\s+|\s+$", "", x, flags=re.UNICODE), ad[element])
                ad["url"] = listing_page_url
            result.append(ad)
            #print(result)
    return json.dumps(result)