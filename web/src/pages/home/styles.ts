import styled from 'styled-components';

export const Main = styled.div`
    margin-right: 30px;
    margin-left: 30px;

`
export const Listing = styled.a`
    display: flex;
    flex-direction: column;
    align-items: left;
    border: 2px solid blue;
    margin-bottom: 30px;
    border-radius: 25px;
    text-decoration: none;
    color: inherit;

    :visited {
        color: grey;
    }
`

export const SecondLine = styled.div`
    display: flex;
`

export const ThirdLine = styled.div`
    display: flex;
`

export const Title = styled.p`
    margin: 0;

`

export const Price = styled.p`
    font-size: 1.3rem;
    font-weight: bolder;
`

export const Description = styled.p`
    max-height: 30px;
    text-overflow: clip; 


`

export const Location = styled.p`

`

export const Typo = styled.p`

`