import axios, { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import * as S from './styles';

interface Listing {
    caracteristics: string[];
    description: string[];
    image: string[];
    location: string[];
    numberBathrooms: string[];
    price: string[];
    sellerName: string[];
    sellerPhoneNumber: string[];
    title: string[];
    typo: string[];
    url: string;
    utilArea: string[];
}

const Home: React.FC = () => {
    const [listings, setListings] = useState<Listing[]>([]);
    useEffect(() => {
        axios.get<Listing[]>('http://localhost:5000')
        .then((response: AxiosResponse) => {
            console.log(response.data);
            setListings(response.data);
        })
    }, []);

    return (
        <S.Main>
            {listings.map(x => {
                return (
                    <S.Listing href={x.url}>
                        <S.Title>{x.title}</S.Title>
                        <S.SecondLine>
                            
                            <S.Price>{x.price}</S.Price>
                            <S.Location>{x.location}</S.Location>
                            
                        </S.SecondLine>
                        <S.ThirdLine>
                            <S.Typo>{x.typo}</S.Typo>
                        </S.ThirdLine>
                        <S.Description>{x.description}</S.Description>
                    </S.Listing>
                )
            })}
        </S.Main>
    );
}
  
  export default Home;