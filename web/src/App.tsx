import React from 'react';
import './App.css';
import Home from './pages/home/Home';

function App() {
  return (
    <div className="App">
      <header className="App-header"></header>
      <Home></Home>
    </div>
  );
}

export default App;
